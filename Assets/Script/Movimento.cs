﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimento : MonoBehaviour {

	public VirtualJoystick joystick;
	private GameObject player;
	private Rigidbody rgb;
	private Vector3 raio;
	public Vector3 lesma;
	public Vector3 pilar;

	void Start(){
		rgb = GetComponent<Rigidbody> ();
		rgb.constraints = RigidbodyConstraints.FreezeRotation;

	}

	void FixedUpdate () {
		if(joystick.inputVector.x > 0){
			transform.RotateAround (Vector3.zero, Vector3.up, joystick.inputVector.x * (-50) * Time.deltaTime);
			Debug.Log ("X: "+joystick.inputVector.x);
			Debug.Log ("Time: "+Time.deltaTime);
		}

		if( joystick.inputVector.x < 0){
			transform.RotateAround (Vector3.zero, Vector3.down,joystick.inputVector.x * 50 * Time.deltaTime);
			Debug.Log ("X: " + joystick.inputVector.x);
		}

		if(joystick.inputVector.z > 0){
			transform.Translate (0, (3 * Time.deltaTime),0);
			Debug.Log ("UP: "+joystick.inputVector.z);
		}

		if(joystick.inputVector.z < 0){
			transform.Translate (0, (-3 * Time.deltaTime),0);
			Debug.Log ("UP: "+joystick.inputVector.z);
		}

	}

	float RaioPilar()
	{
		lesma = GetComponent<Transform> ().position;
		pilar = Vector3.zero;

        raio.z = lesma.z - pilar.z;
		

		Debug.Log ("raio =" + raio);
        return raio.z;
	}

	private void OnCollisionEnter(Collision collider){
		int count;
		//Debug.Log ("Colliding"+count.ToString());
		/*Se o debug ficar soltando mensagem de colliding mesmo sem eu usar input do joystick, então fudeo.
		mas se quando eu parar de usar o joystick o debug parar de mandar a msg, então eu vou tentar inserir o valor da posição do player na variavel de posição do player,
		pra ele não sair do lugar enquanto estiver colidindo */
		//count++;
		rgb.rotation = Quaternion.identity;

	}

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Connection : MonoBehaviour {
    ConnectionConfig config;
    int myReiliableChannelId;
    HostTopology topology;
    int hostId;

    void NewNet() {
        NetworkTransport.Init();
        config = new ConnectionConfig();

        myReiliableChannelId = config.AddChannel(QosType.Reliable);

        topology = new HostTopology(config, 4);
    }

    void NewHost() {
        hostId = NetworkTransport.AddHost(topology, 8888);
    }
    /*
    void NewClient() {
        int connectionId = NetworkTransport.Connect(hostId, "192.16.7.21", 8888, 0, out error);
        NetworkTransport.Disconnect(hostId, connectionId, out error);
        NetworkTransport.Send(hostId, connectionId, myReiliableChannelId, buffer, bufferLength, out error);
    }*/
}

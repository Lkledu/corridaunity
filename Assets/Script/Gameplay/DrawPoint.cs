﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DrawPoint_Util : MonoBehaviour {
    /*
     FUNÇÃO DESENHA LINHAS QUE SE ENCONTRAM EM UM PONTO EM COMUM.
         */
    
    public Vector3 point;
    public Vector3 endX;
    public Vector3 endY;
    public Vector3 endZ;

    void Update () {
        Debug.DrawLine(point, endX, Color.red);
        Debug.DrawLine(point, endY, Color.green);
        Debug.DrawLine(point, endZ, Color.blue);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

	public VirtualJoystick joystick;
	private GameObject player;
	private Rigidbody rgb;
	public Transform ponto;
	private float acceleration = 100;
    Vector3 rotPlayer;

    void Start(){
		rgb = GetComponent<Rigidbody> ();
		rgb.constraints = RigidbodyConstraints.FreezeRotation;

	}

	void Update () {
        //rgb.transform.Translate((ponto.position - transform.position).normalized * acceleration);
		rgb.AddForce((ponto.position - transform.position).normalized * acceleration);

		if(joystick.inputVector.x > 0){
            //transform.RotateAround (Vector3.zero, Vector3.up, joystick.inputVector.x * (-50) * Time.deltaTime);
            
            float xPos= player.transform.position.x + Mathf.Cos(((Vector3.zero.x - player.transform.position.x)/2)* Vector3.zero.x - player.transform.position.x);
            float yPos = player.transform.position.y + Mathf.Sin(((Vector3.zero.x - player.transform.position.x)/ (Vector3.zero.x - player.transform.position.x)) * (Vector3.zero.x - player.transform.position.x));
            
            rotPlayer.x = xPos;
            rotPlayer.y = yPos;
            transform.localPosition = new Vector3 (xPos + joystick.inputVector.x * Time.deltaTime, yPos + joystick.inputVector.x * Time.deltaTime);

			Debug.Log ("X: "+joystick.inputVector.x);
            //Debug.Log("xPos: " +xPos);
            //Debug.Log("yPos: " + yPos);
		}

		if( joystick.inputVector.x < 0){
			transform.RotateAround (Vector3.zero, Vector3.down,joystick.inputVector.x * 50 * Time.deltaTime);
			Debug.Log ("X: " + joystick.inputVector.x);
		}

		if(joystick.inputVector.z > 0){
			transform.Translate (0, (3 * Time.deltaTime),0);
			Debug.Log ("UP: "+joystick.inputVector.z);
		}

		if(joystick.inputVector.z < 0){
			transform.Translate (0, (-3 * Time.deltaTime),0);
			Debug.Log ("UP: "+joystick.inputVector.z);
		}

	}

	private void OnCollisionEnter(Collision collider){
		//int count;
		//Debug.Log ("Colliding"+count.ToString());
		/*Se o debug ficar soltando mensagem de colliding mesmo sem eu usar input do joystick, então fudeo.
		mas se quando eu parar de usar o joystick o debug parar de mandar a msg, então eu vou tentar inserir o valor da posição do player na variavel de posição do player,
		pra ele não sair do lugar enquanto estiver colidindo */
		//count++;
		rgb.rotation = Quaternion.identity;

	}

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerWin : MonoBehaviour {

	private bool showWin = false;
	void OnTriggerEnter(Collider other){
		showWin = true;
		Debug.Log ("win");
	}

	void OnGUI(){
		if (showWin) {
			GUILayout.Box("WIN");
		}
	}

}

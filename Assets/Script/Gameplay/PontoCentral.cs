﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PontoCentral : MonoBehaviour {

	public VirtualJoystick joystick;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(joystick.inputVector.z > 0){
			transform.Translate (0, (3 * Time.deltaTime),0);
			Debug.Log ("UP: "+joystick.inputVector.z);
		}

		if(joystick.inputVector.z < 0){
			transform.Translate (0, (-3 * Time.deltaTime),0);
			Debug.Log ("UP: "+joystick.inputVector.z);
		}
		
	}
}

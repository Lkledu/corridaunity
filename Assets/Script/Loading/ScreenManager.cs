﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenManager : MonoBehaviour 
{
	[Header("Fade In/Out")]
	[SerializeField]
	private Animator m_FadeAnimator;

	[SerializeField]
	private float m_TrasitionTime = 1.0f;

	[Header("Loading")]
	[SerializeField]
	private Animator m_LoadAnimator;

	[SerializeField]
	private float m_LoadingTime = 3.0f;

	private bool m_FirstLoading = true;

	private static ScreenManager m_Instance = null;

	private string nextSceneName = string.Empty;

	public static ScreenManager Instance
	{
		get { return m_Instance; }
	}

	private void Awake()
	{
		if (m_Instance == null)
			m_Instance = this;
		else
			Destroy (gameObject);
	}

	private void OnEnable()
	{
		SceneManager.activeSceneChanged += OnSceneWasLoaded;
	}

	private void OnDisable()
	{
		SceneManager.activeSceneChanged -= OnSceneWasLoaded;
	}

	private void OnSceneWasLoaded(Scene result, Scene checkSceneLoaded)
	{
		if (checkSceneLoaded.isLoaded && checkSceneLoaded.name.Equals(nextSceneName)) 
		{
			StartCoroutine (ExitFade ());
		}
	}

	private IEnumerator ExitFade()
	{
		nextSceneName = string.Empty;
		if (!m_FirstLoading) {
			m_LoadAnimator.SetTrigger ("Fade");
			yield return new WaitForSeconds (m_TrasitionTime * 0.6f);
		}
		m_FadeAnimator.SetTrigger("Fade");
	}

	private IEnumerator ExecuteFade (string sceneName) 
	{
		if (m_FirstLoading) {
			m_FirstLoading = !m_FirstLoading;
		}

		// Chamar Fade Out
		m_FadeAnimator.SetTrigger("Fade");
		yield return new WaitForSeconds (m_TrasitionTime * 0.6f);

		m_LoadAnimator.GetComponent<RandomImageUI> ().Change ();
		m_LoadAnimator.SetTrigger("Fade");
		yield return new WaitForSeconds (m_LoadingTime);

		// Trocar scene
		SceneManager.LoadScene(sceneName);

		yield return null;
	}

	public void LoadScene(string sceneName)
	{
		nextSceneName = sceneName;
		StartCoroutine (ExecuteFade(sceneName));
	}

}

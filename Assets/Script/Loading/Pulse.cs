﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour {
	//"PRESS START" FICA PULSANDO
	private Transform m_Transform;

	[SerializeField]
	[Range(0.1f, 10.0f)]
	private float m_ScaleFactor = 0.2f;

	[SerializeField]
	[Range(0.1f, 10.0f)]
	private float m_SmoothTime = 6.0f;

	private void Awake()
	{
		m_Transform = GetComponent<Transform>();
	}

	private void Update()
	{
		m_Transform.localScale = Vector3.one * 
			(Mathf.Sin (Time.time * m_SmoothTime) * m_ScaleFactor);
		m_Transform.localScale += Vector3.one;

	}
}

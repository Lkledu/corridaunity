﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomImageUI : MonoBehaviour 
{
	[SerializeField]
	private Sprite[] m_Sprites;

	private Image m_Image;

	private void Start () 
	{
		m_Image = GetComponent<Image> ();
		Change ();
	}

	public void Change()
	{
		int index = Random.Range (0, m_Sprites.Length - 1);
		m_Image.sprite = m_Sprites [index];
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PressAnyKey : MonoBehaviour 
{
	[SerializeField]
	private string m_NextSceneName;

	private void Update()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		if (Input.touchCount > 0)
#else
		if (Input.GetMouseButtonDown(0)) 
#endif
		{
			ScreenManager.Instance.LoadScene (m_NextSceneName);
		}
	}
}

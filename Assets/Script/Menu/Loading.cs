﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loading : MonoBehaviour {

    public GameObject loadingScreenObj;
    public Slider slider;

    AsyncOperation async;

    public void LoadScreen(int level) {
        StartCoroutine(LoadingScreen(level));
    }

    IEnumerator LoadingScreen(int level) {
        loadingScreenObj.SetActive(true);
        async = SceneManager.LoadSceneAsync(level);
        async.allowSceneActivation = false;

        while (async.isDone == false) {
            slider.value = async.progress;
            if (async.progress == 0.9f) {
                slider.value = 1f;
                async.allowSceneActivation = true;
            }
            yield return true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class Scene_trans : MonoBehaviour {

    public Button start_btn, option_btn, more_btn;
    private Loading loadObject;
//    public int lvl;

	void Start () {
		start_btn = GameObject.Find ("Start").GetComponent<Button>();
        option_btn = GameObject.Find ("Option").GetComponent<Button>();
        more_btn = GameObject.Find ("More").GetComponent<Button>();

        //        loadObject = GetComponent<Loading>();

        start_btn.onClick.AddListener (StartGame);
        option_btn.onClick.AddListener (OptionGame);
        more_btn.onClick.AddListener(MoreGame);
    }

	public void StartGame(){
        SceneManager.LoadScene("ScenePlay");
       // loadObject.LoadScreen(3);
	}

    public void OptionGame()
    {
        SceneManager.LoadScene("OptionPlay");
        // loadObject.LoadScreen(3);
    }

    public void MoreGame()
    {
        SceneManager.LoadScene("MorePlay");
        // loadObject.LoadScreen(3);
    }


    /*
     * //Function to quit of game (system call)
    public void Quit_Game() {
        Application.Quit();
    }*/

}
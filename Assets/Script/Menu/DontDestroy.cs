﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : MonoBehaviour {

    private static AudioSource music;
    void Awake()
    {
        DontDestroyOnLoad(this);

        if (music == null)
        {
            music = GetComponent<AudioSource>() ;
        }
        else
        {
            DestroyObject(gameObject.GetComponent<AudioSource>());
        }
    }
}

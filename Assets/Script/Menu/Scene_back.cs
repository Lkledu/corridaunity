﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Scene_back : MonoBehaviour {

	private Button back;

	void Start () {
        back = GetComponent<Button> ();
        back.onClick.AddListener (BackScene);
	}

	public void BackScene(){
		SceneManager.LoadScene ("MainMenu");
	}

	private void OnLevelWasLoades(int level){
		if (level != 0) {
			SetupOtherSceneButtons ();
		}
	}

	private void SetupOtherSceneButtons (){
		GameObject.Find ("MainMenuButton").GetComponent<Button> ().onClick.RemoveAllListeners();
		GameObject.Find ("MainMenuButton").GetComponent<Button> ().onClick.AddListener(BackScene);
	}
}
